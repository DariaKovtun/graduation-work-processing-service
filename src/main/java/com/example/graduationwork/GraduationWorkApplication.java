package com.example.graduationwork;

import com.example.graduationwork.util.SampleDataGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@RequiredArgsConstructor
@SpringBootApplication
public class GraduationWorkApplication implements CommandLineRunner {
	private final SampleDataGenerator sampleDataGenerator;

	@Autowired
	private ConfigurableApplicationContext context;

	public static void main(String[] args) {
		SpringApplication.run(GraduationWorkApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		sampleDataGenerator.generateAndStream();
		System.exit(SpringApplication.exit(context));
	}
}
