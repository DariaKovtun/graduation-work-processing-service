package com.example.graduationwork.config;

import com.example.graduationwork.domain.repository.FilterDataRepository;
import com.example.graduationwork.service.CountingBloomFilterAbstractFactory;
import com.example.graduationwork.service.CountingBloomFilterFactory;
import com.example.graduationwork.service.FilterFactory;
import com.example.graduationwork.service.StreamingDataHandler;
import com.example.graduationwork.util.HashUtils;
import com.example.graduationwork.util.PearsonHash;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;
import org.apache.commons.codec.digest.MurmurHash3;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;


@Configuration
public class ApplicationConfiguration {
    private static final int EXPECTED_INPUT = 10000;
    private static final double FALSE_POSITIVE_PROBABILITY = 0.001;
//    CRC-32 (Cyclic redundancy checks)
//    Adler-32 (Checksums)
//    Pearson hashing (Non-cryptographic hash functions)
//    MurmurHash3 (Non-cryptographic hash functions)
//    MD5 (Unkeyed cryptographic hash functions)
//    SHA3-512 (Unkeyed cryptographic hash functions)
//
//
    @Bean
    public List<Function<Integer, Integer>> hashFunctionsPool() {
        List<Function<Integer, Integer>> pool = new ArrayList<>();

        PearsonHash pearsonHash = HashUtils.pearsonHash();

        pool.add(MurmurHash3::hash32);
        pool.add(intToInt(HashUtils.messageDigestHash(MessageDigestAlgorithms.SHA3_512)));
        pool.add(intToInt(HashUtils.messageDigestHash(MessageDigestAlgorithms.MD5)));
        pool.add(HashUtils::adler32);
        pool.add(HashUtils::crc32);
        pool.add(number -> pearsonHash.hash(intToByteArray(number)));
        pool.add(number -> ((number << 2) - 31) ^ (number >>> 3));
        pool.add(intToInt(HashUtils::blake512Hash));
        pool.add(intToInt(HashUtils::sipHash));
        pool.add(intToInt(HashUtils::ripemd256));

        return pool;
    }

    private byte[] intToByteArray(int number) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.putInt(number);
        return byteBuffer.array();
    }

    private int byteArrayToInt(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        return byteBuffer.getInt();
    }

    private Function<Integer, Integer> intToInt(Function<byte[], byte[]> hashFunction) {
        Function<Integer, byte[]> toBytes = this::intToByteArray;
        Function<byte[], Integer> toInt = this::byteArrayToInt;
        return toBytes.andThen(hashFunction).andThen(toInt);
    }

    @Bean
    public CountingBloomFilterFactory<Integer> countingBloomFilter(List<Function<Integer, Integer>> hashFunctionsPool) {
        return new CountingBloomFilterAbstractFactory<Integer>()
                .expectedInputs(EXPECTED_INPUT)
                .falsePositiveProbability(FALSE_POSITIVE_PROBABILITY)
                .setHashFunctionsPool(hashFunctionsPool)
                .newInstance();
    }

    @Bean
    public StreamingDataHandler<Integer> streamingDataHandler(FilterDataRepository filterDataRepository,
                                                              FilterFactory<Integer> filterFactory,
                                                              @Value("${filter.max-amount}") int objectsPerFilterMaxAmount,
                                                              @Value("${filter.timeout}") int filterTimeout) {
        return new StreamingDataHandler<>(
                filterDataRepository,
                filterFactory,
                objectsPerFilterMaxAmount,
                filterTimeout
        );
    }
}
