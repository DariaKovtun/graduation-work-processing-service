package com.example.graduationwork.domain.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter(AccessLevel.PROTECTED)
@Entity
@Table(name = "filter_data")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@TypeDef(name = "list-array", typeClass = ListArrayType.class)
public class FilterData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Type(type = "list-array")
    @Column(columnDefinition = "int[]", nullable = false)
    private List<Integer> filter;
    @Column(name = "start_date", columnDefinition = "timestamp", nullable = false)
    private LocalDateTime startDate;
    @Column(name = "end_date", columnDefinition = "timestamp", nullable = false)
    private LocalDateTime endDate;
    @Column(name = "amount_of_objects", nullable = false)
    private int objectsAmount;
    @Column(name = "distinctive_objects")
    private Integer distinctObjectsAmount;
    @Column(name = "real_distinctive_objects")
    private Integer distinctObjectsRealAmount;

    @Builder
    public FilterData(List<Integer> filter,
                      LocalDateTime startDate,
                      LocalDateTime endDate,
                      int objectsAmount,
                      Integer distinctObjectsAmount,
                      Integer distinctObjectsRealAmount) {
        this.filter = filter;
        this.startDate = startDate;
        this.endDate = endDate;
        this.objectsAmount = objectsAmount;
        this.distinctObjectsAmount = distinctObjectsAmount;
        this.distinctObjectsRealAmount = distinctObjectsRealAmount;
    }

    @Override
    public String toString() {
        return "FilterData{" +
                "id=" + id +
                ", filter='" + filter + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", objectsAmount=" + objectsAmount +
                ", distinctObjectsAmount=" + distinctObjectsAmount +
                ", distinctObjectsRealAmount=" + (distinctObjectsRealAmount == null ? "null" : distinctObjectsRealAmount) +
                '}';
    }
}
