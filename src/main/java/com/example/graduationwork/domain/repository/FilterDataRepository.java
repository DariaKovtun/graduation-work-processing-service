package com.example.graduationwork.domain.repository;

import com.example.graduationwork.domain.model.FilterData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilterDataRepository extends JpaRepository<FilterData, Long> {
}
