package com.example.graduationwork.service;

import lombok.Builder;
import lombok.Singular;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CountingBloomFilter<T> implements Filter<T> {
    private final List<Function<T, Integer>> hashFunctions;
    private final byte[] storage;

    @Builder
    public CountingBloomFilter(@Positive int size,
                               @Singular @NotEmpty List<@NotNull Function<T, Integer>> hashFunctions) {
        this.hashFunctions = hashFunctions;
        this.storage = new byte[size];
        for (int i = 0; i < size; i++) {
            this.storage[i] = Byte.MIN_VALUE;
        }
    }

    @Override
    public int size() {
        return storage.length;
    }

    // объект прогоняется по списку хэш-функций, результат хэш-функций — индекс в блум-фильтре
    @Override
    public void put(@NotNull T object) {
        hashFunctions.forEach(hashFunction -> {
            int key = hashFunction.apply(object) % storage.length;
            key = Math.abs(key);
            storage[key] += 1;
        });
    }

    // приводит хранилище к стандартному значению '0' вместо '-128'
    @Override
    public List<Integer> getFilterRepresentation() {
        List<Integer> filter = new ArrayList<>();
        for (byte value: storage) {
            int intValue = value - Byte.MIN_VALUE;
            filter.add(intValue);
        }
        return filter;
    }
}
