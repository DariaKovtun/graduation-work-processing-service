package com.example.graduationwork.service;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Validated
public class CountingBloomFilterAbstractFactory<T> implements FilterAbstractFactory<T, CountingBloomFilterFactory<T>> {
    private List<Function<T, Integer>> hashFunctionsPool;
    private int expectedInputs;
    private double falsePositiveProbability;

    @Override
    public CountingBloomFilterAbstractFactory<T> addHashFunction(@NotNull Function<T, Integer> hashFunction) {
        ensureHashFunctionsPool();
        hashFunctionsPool.add(hashFunction);
        return this;
    }

    @Override
    public CountingBloomFilterAbstractFactory<T>
    setHashFunctionsPool(@NotEmpty List<@NotNull Function<T, Integer>> hashFunctionsPool) {
        this.hashFunctionsPool = hashFunctionsPool;
        return this;
    }

    @Override
    public CountingBloomFilterAbstractFactory<T> expectedInputs(@Positive int count) {
        this.expectedInputs = count;
        return this;
    }

    @Override
    public FilterAbstractFactory<T, CountingBloomFilterFactory<T>>
    falsePositiveProbability(@DecimalMin("0.0") @DecimalMax("1.0") double probability) {
        falsePositiveProbability = probability;
        return this;
    }

    @Override
    public CountingBloomFilterFactory<T> newInstance() {
        int filterSize = calculateFilterSize();
        int optimalHashFunctionsCount = calculateOptimalHashFunctionsCount(filterSize);
        if (hashFunctionsPool.size() < optimalHashFunctionsCount) {
            throw new IllegalStateException(String.format(
                    "Not enough hash functions for expectedInput=%d and falsePositive=%1.4f. Needed %d functions.",
                    expectedInputs,
                    falsePositiveProbability,
                    optimalHashFunctionsCount
            ));
        }
        List<Function<T, Integer>> hashFunctions = hashFunctionsPool.subList(0, optimalHashFunctionsCount);
        return new CountingBloomFilterFactory<>(filterSize, hashFunctions);
    }

    private void ensureHashFunctionsPool() {
        if (hashFunctionsPool == null) {
            hashFunctionsPool = new ArrayList<>();
        }
    }

    private int calculateFilterSize() {
        double denominator = Math.log(2.);
        denominator *= denominator;
        double sizeFactor = expectedInputs * Math.log(falsePositiveProbability) / denominator;
        return (int) (-1L * Math.round(sizeFactor));
    }

    private int calculateOptimalHashFunctionsCount(int filterSize) {
        return (int) (Math.round(((double) filterSize / expectedInputs) * Math.log(2.)));
    }
}
