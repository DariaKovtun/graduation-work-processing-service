package com.example.graduationwork.service;

import java.util.List;
import java.util.function.Function;

public class CountingBloomFilterFactory<T> implements FilterFactory<T> {
    private final List<Function<T, Integer>> hashFunctions;
    private final int filterSize;

    public CountingBloomFilterFactory(int filterSize, List<Function<T, Integer>> hashFunctions) {
        this.hashFunctions = hashFunctions;
        this.filterSize = filterSize;
    }

    @Override
    public CountingBloomFilter<T> newFilter() {
        return new CountingBloomFilter<>(filterSize, hashFunctions);
    }
}
