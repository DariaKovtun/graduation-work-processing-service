package com.example.graduationwork.service;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Описывает основную функциональность фильтра.
 */
public interface Filter<T> {
    /**
     * Возвращает размер фильтра (количество ячеек).
     *
     * @return      размер фильтра
     */
    int size();

    /**
     * Регистрирует объект в фильтре с использованием хэш-функций.
     * В зависимости от реализации могут использоваться разные стратегии.
     *
     * @param t     регистрируемый объект
     */
    void put(@NotNull T t);

    /**
     * Возвращает строковое представление фильтра.
     * (здесь будет пример)
     *
     * @return      строковое представление
     */
    List<Integer> getFilterRepresentation();
}
