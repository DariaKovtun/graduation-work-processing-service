package com.example.graduationwork.service;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.function.Function;

/**
 * Абстрактная фабрика для создания фабрик, использующихся для создания фильтров.
 *
 * @param <T>       тип данных, регистрируемых фильтром
 * @param <F>       тип фабрики для создания фильтров
 */
public interface FilterAbstractFactory<T, F extends FilterFactory<T>> {
    /**
     * Добавляет новую хэш-функцию в список хэш-функций, которые будут использоваться при создании фабрики фильтра.
     *
     * @param hashFunction      хэш-функция для объектов типа {@link T}.
     * @return                  текущий объект абстрактной фабрики.
     */
    FilterAbstractFactory<T, F> addHashFunction(@NotNull Function<T, Integer> hashFunction);

    /**
     * Устанавливает список хэш-функций, который будет использоваться при создании фабрики фильтров.
     *
     * @param hashFunctionsPool список хэш-функций для объектов типа {@link T}.
     * @return                  текущий объект абстрактной фабрики.
     */
    FilterAbstractFactory<T, F> setHashFunctionsPool(@NotEmpty List<@NotNull Function<T, Integer>> hashFunctionsPool);

    /**
     * Устанавливает максимальное количество входных данных для одного фильтра (в штуках элементов).
     *
     * @param count             максимальное количество.
     * @return                  текущий объект абстрактной фабрики.
     */
    FilterAbstractFactory<T, F> expectedInputs(@Positive int count);

    /**
     * Устанавливает долю ложноположительных срабатываний. Диапазон значений: 0–1.
     *
     * @param probability       доля ложноположительных срабатываний.
     * @return                  текущий объект абстрактной фабрики.
     */
    FilterAbstractFactory<T, F> falsePositiveProbability(@DecimalMin("0.0") @DecimalMax("1.0") double probability);

    /**
     * Создаёт новую фабрику фильтров с конфигурацией, настраивающейся методами:
     * <ul>
     *     <li>{@link FilterAbstractFactory#addHashFunction(Function)}</li>
     *     <li>{@link FilterAbstractFactory#setHashFunctionsPool(List)}</li>
     *     <li>{@link FilterAbstractFactory#expectedInputs(int)}</li>
     *     <li>{@link FilterAbstractFactory#falsePositiveProbability(double)}</li>
     * </ul>
     *
     * @return                  фабрика фильтров.
     */
    F newInstance();
}
