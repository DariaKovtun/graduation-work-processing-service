package com.example.graduationwork.service;

import java.util.List;
import java.util.function.Function;

/**
 * Фабрика для создания фильтров для объектов типа {@link T} с одинаковыми настройками.
 *
 * @param <T>       тип объекта, с которым работает фильтр.
 */
public interface FilterFactory<T> {
    /**
     * Создаёт новый объект фильтра.
     *
     * @return      новый фильтр.
     */
    Filter<T> newFilter();
}
