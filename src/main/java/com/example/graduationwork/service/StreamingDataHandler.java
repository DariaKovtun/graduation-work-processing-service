package com.example.graduationwork.service;

import com.example.graduationwork.domain.model.FilterData;
import com.example.graduationwork.domain.repository.FilterDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Сервис для обработки входящего потока данных.
 * <ol>
 *     <li>В фильтр записываются значения уникальных (?) объектов после обработки хэш-функциями.</li>
 *     <li>При достижении заданного максимального количества элементов, обрабатываемых фильтром (либо по
 *     истечении таймера) содержимое фильтра вместе с его параметрами ("фичами") записывается в базу данных.</li>
 * </ol>
 */
@Validated
public class StreamingDataHandler<T> {

    private final FilterDataRepository filterDataRepository;
    private final FilterFactory<T> filterFactory;
    private final int objectsPerFilterMaxAmount;
    private final int filterTimeout;

    private AtomicInteger counter;
    private Set<T> distinctItems;
    private volatile LocalDateTime currentFilterStartTime;
    private volatile Filter<T> currentFilter;
    private volatile Timer filterTimer;

    /**
     *
     * @param filterDataRepository      репозиторий для записи в базу данных.
     * @param filterFactory             фабрика для создания фильтров.
     * @param objectsPerFilterMaxAmount максимальное количество элементов, обрабатываемое фильтром.
     * @param filterTimeout             таймер (в секундах) максимальной длины жизни фильтра.
     */
    public StreamingDataHandler(@NotNull FilterDataRepository filterDataRepository,
                                @NotNull FilterFactory<T> filterFactory,
                                @Positive int objectsPerFilterMaxAmount,
                                @Positive int filterTimeout) {
        this.filterDataRepository = filterDataRepository;
        this.filterFactory = filterFactory;
        this.objectsPerFilterMaxAmount = objectsPerFilterMaxAmount;
        this.filterTimeout = filterTimeout;
    }

    /**
     *
     *
     * @param data      поток входящих данных типа {@link T}.
     * @return          возвращает пустой поток данных в случае успеха
     */
    public Mono<Void> handle(Flux<T> data) {
        // создаётся фильтр, если его нет
        if (currentFilter == null) {
            init();
            updateFilter();
        }
        return data
                .doOnNext(this::storeItemInFilter)
                .map(item -> counter.incrementAndGet())
                // в случае, если счётчик достиг максимального значения:
                .filter(currentCounter -> currentCounter == objectsPerFilterMaxAmount)
                .doOnNext(ignore -> {
                    // 1. фоновая задача отменяется
                    // 2. фильтр записывается в базу данных
                    // 3. фильтр обновляется
                    filterTimer.cancel();
                    storeFilterInDb();
                    updateFilter();
                    System.out.println("Filter stored");
                })
                .then();
    }

    private void init() {
        Map<T, Boolean> map = new ConcurrentHashMap<>();
        distinctItems = Collections.newSetFromMap(map);
        counter = new AtomicInteger(0);
    }

    // 1. создаётся новый фильтр через фабрику
    // 2. создаётся новое хранилище уникальных элементов (сет)
    // 3. обнуляется счётчик элементов в фильтре
    // 4. создаётся фоновая задача на исполнение по завершению жизни фильтра
    private void updateFilter() {
        currentFilter = filterFactory.newFilter();
        counter.set(0);
        distinctItems.clear();
        currentFilterStartTime = LocalDateTime.now();
        TimerTask filterTimeoutTask = new FilterTimeoutTask(currentFilterStartTime);
        filterTimer = new Timer("Filter_" + currentFilterStartTime.toString());
        filterTimer.schedule(filterTimeoutTask, filterTimeout * 1000L);
    }

    // элемент записывается в фильтр и хранилище уникальных элементов (сет)
    private void storeItemInFilter(T item) {
        currentFilter.put(item);
        distinctItems.add(item);
    }

    private void storeFilterInDb() {
        LocalDateTime endTime = LocalDateTime.now();
        FilterData filterData = FilterData.builder()
                .filter(currentFilter.getFilterRepresentation())
                .objectsAmount(counter.get())
                .distinctObjectsRealAmount(distinctItems.size())
                .startDate(currentFilterStartTime)
                .endDate(endTime)
                .build();
        filterDataRepository.saveAndFlush(filterData);
    }

    @RequiredArgsConstructor
    private class FilterTimeoutTask extends TimerTask {
        private final LocalDateTime filterStartTime;

        /**
         * 1. Проверка на то, что задача запустилась на тот фильтр, который нужен
         * 2. Проверка на то, сколько секунд прошло
         * 3. Если прошло 60 секунд, фильтр сохраняется в базу данных
         * 4. Фильтр обновляется
         */
        @Override
        public void run() {
            if (filterStartTime.equals(StreamingDataHandler.this.currentFilterStartTime)) {
                long delta = filterStartTime.until(LocalDateTime.now(), ChronoUnit.SECONDS);
                if (delta >= StreamingDataHandler.this.filterTimeout) {
                    StreamingDataHandler.this.storeFilterInDb();
                    StreamingDataHandler.this.updateFilter();
                }
            }
        }
    }
}
