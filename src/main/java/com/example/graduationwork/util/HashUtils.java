package com.example.graduationwork.util;

import lombok.SneakyThrows;
import org.bouncycastle.crypto.macs.SipHash;
import org.bouncycastle.jcajce.provider.digest.Blake2b;
import org.bouncycastle.jcajce.provider.digest.RIPEMD256;

import java.security.MessageDigest;
import java.util.function.Function;
import java.util.zip.Adler32;
import java.util.zip.CRC32;

public class HashUtils {

    public static byte[] blake512Hash(byte[] numberBytes) {
        Blake2b.Blake2b512 blake2b512 = new Blake2b.Blake2b512();
        return blake2b512.digest(numberBytes);
    }

    public static byte[] sipHash(byte[] bytes) {
        SipHash sipHash = new SipHash();
        byte[] out = new byte[bytes.length * 2];
        sipHash.update(bytes, 0, bytes.length);
        sipHash.doFinal(out, 0);
        return out;
    }

    public static PearsonHash pearsonHash() {
        return new PearsonHash();
    }

    public static Function<byte[], byte[]> messageDigestHash(String algorithm) {
        return bytes -> digestWith(algorithm, bytes);
    }

    public static int adler32(int number) {
        Adler32 adler32 = new Adler32();
        adler32.update(number);
        return (int) adler32.getValue();
    }

    public static int crc32(int number) {
        CRC32 crc32 = new CRC32();
        crc32.update(number);
        return (int) crc32.getValue();
    }

    public static byte[] ripemd256(byte[] bytes) {
        RIPEMD256.Digest ripemd256 = new RIPEMD256.Digest();
        return ripemd256.digest(bytes);
    }

    @SneakyThrows
    private static byte[] digestWith(String algorithm, byte[] bytes) {
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        return messageDigest.digest(bytes);
    }
}
