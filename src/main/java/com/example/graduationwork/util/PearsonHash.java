package com.example.graduationwork.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PearsonHash {
    private final List<Integer> table;

    public PearsonHash() {
        table = new ArrayList<>();
        initTable();
    }

    public int hash(byte[] bytes) {
        int hash;
        int result = 0;
        for (int i = 0; i < 4; i++) {
            int unsignedByte = Byte.toUnsignedInt(bytes[0]);
            hash = table.get((unsignedByte + i) % 256);
            for (byte numberByte : bytes) {
                unsignedByte = Byte.toUnsignedInt(numberByte);
                hash = table.get(hash ^ unsignedByte);
            }
            result = ((result << 8) | hash);
        }
        return result;
    }

    private void initTable() {
        int hash;
        int result = 0;
        for (byte i = 0; i < Byte.MAX_VALUE; i++) {
            table.add(Byte.toUnsignedInt(i));
        }
        table.add(Byte.toUnsignedInt(Byte.MAX_VALUE));
        for (byte i = Byte.MIN_VALUE; i < 0; i++) {
            table.add(Byte.toUnsignedInt(i));
        }
        Collections.shuffle(table);
    }
}
