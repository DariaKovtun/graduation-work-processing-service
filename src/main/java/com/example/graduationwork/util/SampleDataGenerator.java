package com.example.graduationwork.util;

import com.example.graduationwork.service.StreamingDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.stream.IntStream;

@Service
public class SampleDataGenerator {
    private final StreamingDataHandler<Integer> streamingDataHandler;
    private final int maxDelay;
    private final int minDelay;
    private final int batchSize;
    private final int batchesCount;
    private final int baseBound;
    private final int boundGrowthRate;

    public SampleDataGenerator(@Autowired StreamingDataHandler<Integer> streamingDataHandler,
                               @Value("${sample.max-delay}") int maxDelay,
                               @Value("${sample.min-delay}") int minDelay,
                               @Value("${sample.batch-size}") int batchSize,
                               @Value("${sample.batches-count}") int batchesCount,
                               @Value("${sample.base-bound}") int baseBound,
                               @Value("${sample.bound-growth-rate}") int boundGrowthRate) {
        this.streamingDataHandler = streamingDataHandler;
        this.maxDelay = maxDelay;
        this.minDelay = minDelay;
        this.batchSize = batchSize;
        this.batchesCount = batchesCount;
        this.baseBound = baseBound;
        this.boundGrowthRate = boundGrowthRate;
    }

    public void generateAndStream() {
        Random random = new Random();
        IntStream source = IntStream.range(0, batchSize)
                .peek(this::printEvery10_000)
                .map(i -> getRandomInt(random, baseBound));
        Flux<Integer> stream = delay(random, source);
        for (int i = 1; i < batchesCount; i++) {
            int rate = ((i % boundGrowthRate) + 1);
            int baseBound = rate * batchesCount;
            source = IntStream.range(0, batchSize)
                    .peek(this::printEvery10_000)
                    .map(j -> getRandomInt(random, baseBound));
            stream = stream.concatWith(delay(random, source));
        }
        streamingDataHandler.handle(stream).block();
    }

    private Flux<Integer> delay(Random random, IntStream source) {
        int amount = random.nextInt(maxDelay) + minDelay;
        Duration delay = Duration.of(amount, ChronoUnit.NANOS);
        System.out.println("Delay (nano): " + amount);
        return Flux.fromStream(source.boxed())
                .delayElements(delay);
    }

    private void printEvery10_000(int currentI) {
        if (currentI % 10_000 == 0) {
            System.out.println("Emitted 10_000 numbers");
        }
    }

    private int getRandomInt(Random random, int baseBound) {
        int lower = random.nextInt(baseBound);
        int upper = random.nextInt(baseBound) + lower + 1;
        return random.nextInt(upper) + lower;
    }
}
