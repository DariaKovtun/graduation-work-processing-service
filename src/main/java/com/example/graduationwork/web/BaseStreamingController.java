package com.example.graduationwork.web;

import com.example.graduationwork.service.StreamingDataHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class BaseStreamingController<T> {
    private final StreamingDataHandler<T> streamingDataHandler;

    protected final Mono<ResponseEntity<Void>> handle(Mono<T> streamingData) {
        return streamingDataHandler.handle(streamingData.flux())
                .thenReturn(ResponseEntity.ok().build());
    }

    protected final Mono<ResponseEntity<Void>> handle(Flux<T> streamingData) {
        return streamingDataHandler.handle(streamingData)
                .thenReturn(ResponseEntity.accepted().build());
    }
}
