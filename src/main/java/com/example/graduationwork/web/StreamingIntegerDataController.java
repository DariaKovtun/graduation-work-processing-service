package com.example.graduationwork.web;

import com.example.graduationwork.service.StreamingDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping(path = "/data")
public class StreamingIntegerDataController extends BaseStreamingController<Integer> {

    public StreamingIntegerDataController(@Autowired StreamingDataHandler<Integer> streamingDataHandler) {
        super(streamingDataHandler);
    }

    @PostMapping(path = "/single")
    public Mono<ResponseEntity<Void>> postSingleData(Mono<Integer> data) {
        return handle(data);
    }

    @PostMapping(path = "/stream")
    public Mono<ResponseEntity<Void>> postDataStream(Flux<Integer> data) {
        return handle(data);
    }
}
