package com.example.graduationwork;

import com.example.graduationwork.domain.model.FilterData;
import com.example.graduationwork.domain.repository.FilterDataRepository;
import com.example.graduationwork.service.StreamingDataHandler;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class StreamingDataHandlerIntegrationTest {
    @Autowired
    private StreamingDataHandler<Integer> streamingDataHandler;
    @Autowired
    private FilterDataRepository repository;

    @AfterEach
    public void clear() {
        repository.deleteAll();
    }

    @Test
    public void testAllUnique() {
        Flux<Integer> stream = Flux.fromStream(IntStream.range(0, 100000).boxed());
        streamingDataHandler.handle(stream).subscribe();
        List<FilterData> filterDataList = repository.findAll();
        assertThat(filterDataList.size(), is(10));
        assertThat(filterDataList, everyItem(hasProperty("distinctObjectsRealAmount", is(10000))));
    }

    /**
     *
     */
    @Test
    public void test70Unique() {
        IntStream intStream = IntStream.concat(IntStream.range(0, 7000), IntStream.range(0, 3000));
        Flux<Integer> stream = Flux.fromStream(intStream.boxed());
        streamingDataHandler.handle(stream).subscribe();
        List<FilterData> filterDataList = repository.findAll();
        assertThat(filterDataList.size(), is(1));
        FilterData filterData = filterDataList.get(0);
        assertThat(filterData.getDistinctObjectsRealAmount(), is(7000));
    }

    @Test
    public void test50Unique() {
        IntStream intStream = IntStream.concat(IntStream.range(0, 5000), IntStream.range(0, 5000));
        Flux<Integer> stream = Flux.fromStream(intStream.boxed());
        streamingDataHandler.handle(stream).subscribe();
        List<FilterData> filterDataList = repository.findAll();
        assertThat(filterDataList.size(), is(1));
        FilterData filterData = filterDataList.get(0);
        assertThat(filterData.getDistinctObjectsRealAmount(), is(5000));
    }

    @Test
    public void test20Unique() {
        IntStream intStream = IntStream.range(0, 10000)
                .map(number -> number % 2000);
        Flux<Integer> stream = Flux.fromStream(intStream.boxed());
        streamingDataHandler.handle(stream).subscribe();
        List<FilterData> filterDataList = repository.findAll();
        assertThat(filterDataList.size(), is(1));
        FilterData filterData = filterDataList.get(0);
        assertThat(filterData.getDistinctObjectsRealAmount(), is(2000));
    }

    @Test
    public void testAllSame() {
        IntStream intStream = IntStream.range(0, 10000)
                .map(number -> 123456);
        Flux<Integer> stream = Flux.fromStream(intStream.boxed());
        streamingDataHandler.handle(stream).subscribe();
        List<FilterData> filterDataList = repository.findAll();
        assertThat(filterDataList.size(), is(1));
        FilterData filterData = filterDataList.get(0);
        assertThat(filterData.getDistinctObjectsRealAmount(), is(1));
    }
}

